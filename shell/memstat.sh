#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"

while true
do

#RAM
  mapfile -t MEMINFO < <(cat /proc/meminfo | grep Mem | sed -e 's/[^0-9]*//g')

  MEMTOTAL=$((${MEMINFO[0]}/1000))
  MEMUSED=$(($MEMTOTAL-${MEMINFO[2]}/1000))
  PERCENT=$(($MEMUSED*100/$MEMTOTAL))

  printf "$MEMUSED\n$MEMTOTAL\n$PERCENT" > ../www/var/memstat.txt
  
#SWAP
  mapfile -t SWAPINFO < <(cat /proc/meminfo | grep Swap | sed -e 's/[^0-9]*//g')
         
  SWAPTOTAL=$((${SWAPINFO[1]}/1000))
  
  if [ $SWAPTOTAL -ne 0 ]; then
  	SWAPUSED=$(($SWAPTOTAL-${SWAPINFO[2]}/1000))
  	PERCENT=$(($SWAPUSED*100/$SWAPTOTAL))           
  else
  	SWAPUSED=0
  	PERCENT=0	
  fi
 
  printf "$SWAPUSED\n$SWAPTOTAL\n$PERCENT" > ../www/var/swapstat.txt

  sleep 1
done
