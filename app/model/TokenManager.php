<?php

namespace App\Model;

use Nette;
use function Nette\Database\Table\fetch;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Context;
use Nette\Utils\Random;
use Nette\Mail\SendmailMailer;

class TokenManager {
	
	const
		TABLE_NAME = 'activation_tokens',
		COLUMN_ID = 'id',
		COLUMN_TOKEN = 'token',
		COLUMN_USER = 'user',
		COLUMN_EXPIRATION = 'expiration',
		COLUMN_IS_VALID = 'isvalid';
	
	private $database;
		
	public function __construct(Context $database){
		$this->database = $database;
	}	
	
	/**
	 * Creates a new token for user and inserts it into DB
	 * @param $user
	 */
	public function createToken($userId){
	    $token = Random::generate(32);
	    $this->database->table(self::TABLE_NAME)->insert([
	            'token' => Random::generate(32),
	            'user' => $userId,
	            'expiration' => (new \DateTime(date("Y-m-d H:i:s")))->add(new \DateInterval("P1D")),
	    ]);
	}
	
	/**
	 * Sends activation mail to specific user
	 * @param $userId
	 */
	public function sendActivationMail($userId){
	    $token = $this->database->table(self::TABLE_NAME)
	       ->where(self::COLUMN_USER, $userId)
	       ->where(self::COLUMN_IS_VALID, true)
	       ->fetch();
	    
	    $user = $this->database->table('users')
	       ->where('id', $userId)
	       ->fetch();
	    
	       
	    $mail = new \Nette\Mail\Message;
	    $mail->setFrom("mailer@nas.berry")
    	    ->addTo($user['email'])
    	    ->setSubject("Nasberry access granted")
    	    ->setHtmlBody("Click here to see your credentials: <br /> http://" . exec("cat /etc/hostname") . "/token/check?t=" . $token[self::COLUMN_TOKEN]
	            . "<br />If you didn't ask for nasberry webadmin access, feel free to delete this mail :)");
	    $mailer = new \Nette\Mail\SendmailMailer();
	    $mailer->send($mail);
	}
	
	/**
	 * Returns token specified by its value (token column)
	 * @param string
	 * @return Nette\Database\Table\ActiveRow
	 */
	private function getToken($t){
		return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_TOKEN, $t)->fetch();
	}
	
	/**
	 * Checks validity of a token defined by its id, result is returned 
	 * in array alongside user id and error message if any.
	 * @param string
	 * @return array(bool, int, string)
	 */
	public function checkToken($t){
		$token = $this->getToken($t);
		$isValid = false;
		$message = "";
		
		if(!$token ){
			$message = "No such token."; 
		} elseif (!$token[self::COLUMN_IS_VALID]){
			$message = "Token used already.";
		} elseif (strtotime($token[self::COLUMN_EXPIRATION]) < strtotime(date("Y-m-d H:i:s"))){
			$message = "Token expired.";
		} else {
			$isValid = true;
		}
		
		return array($isValid, $token[self::COLUMN_USER], $message);
	}
	
	/**
	 * Marks token specified by its value as invalid
	 * @param string
	 */
	public function discardToken($t){
		$token = $this->getToken($t);
		$token->update([
				self::COLUMN_IS_VALID => false,
			]);
	}
}