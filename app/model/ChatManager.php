<?php

namespace App\Model;

use Nette;
use Nette\Database\Context;

class ChatManager {
    const
        TABLE_NAME = 'chat',
        COLUMN_ID = 'id',
        COLUMN_AUTHOR_ID = 'authorId',
        COLUMN_MESSAGE = 'message',
        COLUMN_TIME = 'time';
    
    private $database;
        
    public function __construct(Context $database){
        $this->database = $database;
    }
    
    public function getLastXMessages($count){
        return $this->database->table(self::TABLE_NAME)
            ->order(self::COLUMN_ID . ' DESC')
            ->limit($count);
    }
    
    public function addNew($authorId, $message){
        $this->database->table(self::TABLE_NAME)
            ->insert([
                    self::COLUMN_AUTHOR_ID => $authorId,
                    self::COLUMN_MESSAGE => $message,
                    self::COLUMN_TIME => new \DateTime(date('Y-m-d H:i:s'))
            ]);
    }
}