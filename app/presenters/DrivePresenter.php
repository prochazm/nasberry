<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Utils\SafeStream;
use App\Model\DriveManager;

class DrivePresenter extends BasePresenter
{
    private $driveManager;
    
    public function __construct(DriveManager $driveManager){
        $this->driveManager = $driveManager;
    }

	private function getDriveTable(){
	    $fileContent = file('./var/drives.txt');
	    for ($i = 0; $i < sizeof($fileContent); ++$i){
	        $fileContent[$i] = preg_split("/\s/", trim($fileContent[$i]), 6);
	    }
	    
	    return $fileContent;
	}
    
    public function actionShow(){
		$this->template->drives = $this->getDriveTable();
	}
	
	public function handleRefresh(){
	    if (!$this->isAjax()){
	        return;
	    }
	    
	    $this->template->drives = $this->getDriveTable();
	    $this->redrawControl('table');
	}
	
	public function handleUmount($mntPoint){
	    if (!$this->isAjax()){
	        return; 
	    }
	    
	    $this->driveManager->setUnmountable(str_replace("/media/", "", $mntPoint));
	    
		if (strpos($mntPoint, '/media/') !== false){
		    //this will need status check in future
    	    exec("sudo ../shell/umount.sh \"" . $mntPoint . "\"");
    	    $this->driveManager->setUnmountable(str_replace("/media/", "", $mntPoint));
		} 
		
	}
	

	
}