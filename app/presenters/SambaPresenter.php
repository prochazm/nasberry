<?php

/**
 * IN PROGRESS
 * THIS IS THE PLACE WHERE SAMBA IS MANAGED (frontend)
 */

namespace App\Presenters;

use Nette;
use Nette\Application\UI;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;
use App\Model\UserManager;
use App\Model\DriveManager;

class SambaPresenter extends BasePresenter{
    private $shares;
    private $userManager;
    private $driveManager;
   
    public function __construct(UserManager $userManager, DriveManager $driveManager){
        $this->userManager = $userManager;
        $this->driveManager = $driveManager;
    }
    
	protected function createComponentAddForm(){
		$form = new Form();
        $form->setRenderer(new BootstrapRenderer());
		
        $users = $this->userManager->getAllNames();
        $drives = $this->driveManager->getAllDrives();
        
		$yesNoArray = [
			'no'  => 'No',
			'yes' => 'Yes',
		];
		
		$form->addText('title', 'Title:')
			->setRequired('Set the title.');
		$form->addText('comment', 'Comment:')
			->setRequired('Comment the share.');
		$form->addSelect('drive', 'Drive:', $drives, 1)
		    ->setRequired('Select drive.');
		$form->addText('path', 'Path:')
		    ->setDefaultValue('/')
			->setRequired('Set the path.');
		$form->addRadioList('browseable', 'Browseable:', $yesNoArray)
			->setDefaultValue('no');
		$form->addRadioList('readOnly', 'Read only:', $yesNoArray)
			->setDefaultValue('no');
		$form->addRadioList('guestOk', 'Guest ok:', $yesNoArray)
			->setDefaultValue('no');
		$form->addMultiSelect('validUsers', 'Valid users', $users, min(sizeof($users), 10));
		$form->addSubmit('add', 'Add');
		
		$form->onSuccess[] = [$this, 'addFormSucceeded'];
		return $form;
	}
	
	public function addFormSucceeded(Form $form, array $values){
		$values['title'] = '[' . $values['title'] . ']';
		$values['path'] = "\"" . $values['path'] . "\"";
		$values['validUsers'] = implode(':', $values['validUsers']); 
		$values['path'] = '/media/' . $values['drive'] . $values['path'];
		unset($values['drive']);
		$args = str_replace(" ", ",", implode('!', $values));
		exec('sudo ../shell/samba.sh -a ' . $args);
		$this->flashMessage($values['title'] . ' added to samba shares.', 'success');
		$this->redirect('Samba:manage');
	}
	
	protected function createComponentRemoveForm(){
		$form = new Form();
		$form->setRenderer(new BootstrapRenderer());
		
		$form->addMultiSelect('share', 'Share:', $this->shares, min(sizeof($this->shares), 20));
		$form->addCheckbox('deleteCheck', 'Really?')
			->setRequired('Check the box if you really want to delete this.');
		$form->addSubmit('remove', 'Remove');
		
		$form->onSuccess[] = [$this, 'removeFormSucceeded'];
		return $form;
	}
	
	public function removeFormSucceeded(Form $form, array $values){
		foreach ($values['share'] as $share){
		    exec('sudo ../shell/samba.sh -r ' . str_replace(' ', ',', $share));
		}
		
		$this->flashMessage('Shares removed: ' . implode(', ', $values['share']), 'success');
		$this->redirect('Samba:manage');
	}
	
	public function actionManage(){
	    exec('../shell/samba.sh -s', $bashOut);
	    $this->shares = array_combine($bashOut, $bashOut);
	    $this->template->hasShares = sizeof($this->shares) > 0;
	}
	
	
}