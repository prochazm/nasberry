#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"

while true
do
  df -h | grep /media/ | sed -e 's/  */ /g' > ../www/var/drives.txt;
  sleep 1;
done
