<?php

namespace App\Presenters;

use Nette;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;
use App\Model\UserManager;
use App\Model\TokenManager;

class PasswordPresenter extends BasePresenter{
    private $userManager;
    private $tokenManager;

    public function __construct(UserManager $userManager, TokenManager $tokenManager){
        $this->userManager = $userManager;
        $this->tokenManager = $tokenManager;
    }

    public function actionReset(){

    }

    protected function createComponentResetForm(){
        $form = new Form();
        $form->setRenderer(new BootstrapRenderer());

        $form->addText('username', 'User: ')
        ->setRequired('*');
        $form->addEmail('emial', 'Email: ')
        ->setRequired('*');
        $form->addSubmit('reset', 'Reset');
        $form->onSuccess[] = [$this, 'resetFormSucceeded'];

        return $form;
    }

    public function resetFormSucceeded(Form $form, array $values){
        list($username, $email) = array_values($values);

        if ($this->userManager->getUserByNameAndMail($username, $email)){
            $id = $this->userManager->getUserByName($username);
            $this->userManager->disable($id);
            $this->tokenManager->createToken($id);
            $this->tokenManager->sendActivationMail($id);

            $this->flashMessage('Reset mail was sent on your email address', 'success');
            $this->redirect('Homepage:');
        }

        $this->flashMessage('Account with such name and email doesn\'t exist', 'danger');
    }
}