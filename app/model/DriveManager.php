<?php

namespace App\Model;

use Nette\Database\Table;
use Nette\Database\Context;

class DriveManager {
    const
        TABLE_NAME = 'drives',
        COLUMN_ID = 'id',
        COLUMN_UUID = 'uuid',
        COLUMN_LABEL = 'label',
        COLUMN_MOUNTABLE = 'mountable',
        COLUMN_MOUNTED = 'mounted';
    
    private $database;
    
    public function __construct(Context $database){
        $this->database = $database;
    }
    
    public function getMounted(){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_MOUNTED, 1)
            ->fetchAll();
    }
    
    public function getUnmountable(){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_MOUNTED, 0)
            ->where(self::COLUMN_MOUNTABLE, 0)
            ->fetchAll();
    }
    
    public function getAllDrives(){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_MOUNTED, '1')
            ->fetchPairs(self::COLUMN_UUID, self::COLUMN_UUID);
    }
    
    public function setUnmountable($uuid){
        $drive = $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_UUID, $uuid)
            ->fetch();
        
        $drive->update([ 
                self::COLUMN_MOUNTABLE => 0
        ]);
    }
}