<?php

namespace App\Security;

use Nette,
    Nette\Security\Permission;

class AuthorizatorFactory extends Nette\Object {
    const 
        ROLE_GUEST = 'guest',
        ROLE_USER = 'user',
        ROLE_ADMIN = 'admin',
        
        RES_SAMBA = 'Samba',
        RES_DRIVE = 'Drive',
        RES_ERROR = 'Error4xx',
        RES_HOME  = 'Homepage',
        RES_SIGN  = 'Sign',
        RES_TOKEN = 'Token',
        RES_APL = 'Application',
        RES_USER = 'User',
        RES_FORGPWD = 'Password',
        RES_LICENCE = 'Licence'
    ;
    
    public function create(){
        $acl = new Permission();
        
        $acl->addRole(self::ROLE_GUEST);
        $acl->addRole(self::ROLE_USER, self::ROLE_GUEST);
        $acl->addRole(self::ROLE_ADMIN, self::ROLE_USER);
        
        $acl->addResource(self::RES_SAMBA);
        $acl->addResource(self::RES_DRIVE);
        $acl->addResource(self::RES_ERROR);
        $acl->addResource(self::RES_HOME);
        $acl->addResource(self::RES_SIGN);
        $acl->addResource(self::RES_TOKEN);
        $acl->addResource(self::RES_APL);
        $acl->addResource(Self::RES_USER);
        $acl->addResource(self::RES_FORGPWD);
        $acl->addResource(self::RES_LICENCE);
        
        $acl->allow(self::ROLE_GUEST, self::RES_HOME, Permission::ALL);
        $acl->allow(self::ROLE_GUEST, self::RES_TOKEN, Permission::ALL);
        $acl->allow(self::ROLE_GUEST, self::RES_ERROR, Permission::ALL);
        $acl->allow(self::ROLE_GUEST, self::RES_FORGPWD, Permission::ALL);
        $acl->allow(self::ROLE_GUEST, self::RES_LICENCE, Permission::ALL);
        $acl->allow(self::ROLE_GUEST, self::RES_SIGN, 'in');
        $acl->allow(self::ROLE_GUEST, self::RES_APL, 'apply');
        
        $acl->allow(self::ROLE_USER, self::RES_SAMBA, Permission::ALL);
        $acl->allow(self::ROLE_USER, self::RES_DRIVE, Permission::ALL);
        $acl->allow(self::ROLE_USER, self::RES_SIGN, 'out');
        $acl->allow(self::ROLE_USER, self::RES_USER, Permission::ALL);
        $acl->deny(self::ROLE_USER, self::RES_FORGPWD, Permission::ALL);
        $acl->deny(self::ROLE_USER, self::RES_SIGN, 'in');
        $acl->deny(self::ROLE_USER, self::RES_APL, 'apply');
        
        $acl->allow(self::ROLE_ADMIN, self::RES_SIGN, 'up');
        $acl->allow(self::ROLE_ADMIN, self::RES_APL, 'show');
        $acl->allow(self::ROLE_ADMIN, self::RES_APL, 'accept');
        
        return $acl;
    }
}