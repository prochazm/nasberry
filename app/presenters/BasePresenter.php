<?php

namespace App\Presenters;

use Nette;
use App\Model\SysbarManager;
use App\Model\UserManager;
use App\Model\UserSettingManager;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{	 
    private $userManager;
    private $userSettingManager;
    
    public function injectUserManager(UserManager $userManager){
        $this->userManager = $userManager;
    }
    
    public function injectUserSettingManager(UserSettingManager $userSettingManager){
        $this->userSettingManager = $userSettingManager;
    }
    
    public function checkRequirements($element){
        if(!$this->user->isAllowed($this->getName(), $this->getAction())){
            if(!$this->user->isLoggedIn()){
                $this->flashMessage('Sign in to access this page.', 'info');
                $this->redirect('Sign:in');
            } 
                
            throw new Nette\Application\ForbiddenRequestException;
        }
    }
    
    private function setSensors(){
        $sysbarManager = new SysbarManager();
        
        $this->template->cpuUsages = $sysbarManager->getCpu();
        $this->template->ramUsage = $sysbarManager->getRam();
        $this->template->swapUsage = $sysbarManager->getSwap();
        $this->template->temp = $sysbarManager->getTemp();
    }
    
    private function setSysbarVisibility(){
        $this->template->isSysbarCollapsed = 
            $this->userSettingManager->isSysbarCollapsed($this->user->id);
    }
    
	protected function beforeRender(){
		parent::beforeRender();
		$this->setSensors();
		$this->setSysbarVisibility();
		
		$this->template->coreCount = sizeof($this->template->cpuUsages);
		$cpuProgressNoCorrection = 12 / $this->template->coreCount;
		$this->template->cpuProgress = ($cpuProgressNoCorrection < 2) ? 2 : $cpuProgressNoCorrection;
		
	}
	
    public function handleSysbar(){
	   if($this->isAjax()){
	        $this->setSensors();
	        $this->redrawControl('sysbar');
	   } else{
			$this->redirect('this');
	   }
	}
	
}
