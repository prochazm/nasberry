#!/bin/bash
# ! line separator
# , space separator
# title has to be in brackets

SMB_PATH="/etc/samba/smb.conf"

remove() {
	SHARE_NAME=$(echo $1 | sed -e "s/,/ /g")
    echo $SMB_PATH | sed -i -e "/\\$SHARE_NAME/,/#END/d" $SMB_PATH
}

case "$1" in
  -a|--add)
    if [[ $2 == global ]] 
    then
      echo "you can't remove global section"
      exit
    fi

    params=("comment" "path" "browseable" "read only" "guest ok" "valid users")
    output=$2
    
    for ((i=0; i < ${#params}; i++)) 
    do
      output=$(printf "$output" | sed -e "0,/!/s/!/\n${params[$i]} = /")
    done
    
    remove $(printf "$output" | sed -n -e '1{p;q}')
    output="$(printf "$output" | sed -e "s/,/ /g" | sed -e "s/:/, /g")\n#END\n"

    printf "$output" >> $SMB_PATH
    /etc/init.d/samba restart
  ;;
  -r|--remove)
    remove "[$2]"
    /etc/init.d/samba restart
  ;;
  -g|--general)
    echo "$0: under development"
  ;;
  -L|--list-all)
    cat $SMB_PATH
  ;;
  -l|--list)
    printf "$(cat $SMB_PATH | sed -n "/\[$2\]/,/#END/p" | head -n -1)"
  ;;
  -s|--short-list)
  	cat /etc/samba/smb.conf | grep -e '^\[.*\]$' | grep -v global | grep -v -e '^\[print.*\]$' | sed -e 's/\[\|\]//g'
  ;;
  *)
    echo "$0: invalid argument, check source or documentation"
  ;;
esac

