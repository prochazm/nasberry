#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )" 

function diff() {
  local start=$(getAt ${START[$1]} $2)
  local end=$(getAt ${END[$1]} $2)
  echo $(($end - $start))
}

function getAt() {
  echo $1 | cut -d '-' -f $2  
}
 
declare -a START
declare -a END 

while true; do
  i=0
  while read -r line
  do
    START[$i]=$line
    ((i++))
  done < <(grep 'cpu[0-9] ' /proc/stat | sed -e 's/  */-/g') 

  sleep 0.5

  i=0
  while read -r line
  do
    END[$i]=$line
    ((i++))
  done < <(grep 'cpu[0-9] ' /proc/stat | sed -e 's/  */-/g')

  for ((i=0; i<${#START[@]}; ++i))
  do
    USR=$(diff $i 2)
    SYS=$(diff $i 4)
    IDLE=$(diff $i 5)
    IOW=$(diff $i 6)
    USAGE=$((($USR+$SYS+$IOW)*100/($USR+$SYS+$IDLE+$IOW)))   
    USAGES[$i]=$USAGE    
  done

  printf "%s\n" "${USAGES[@]}" > ../www/var/cpustat.txt

  sleep 0.5
done
