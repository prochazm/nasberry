#!/bin/bash

case $1 in
  -n | --new)
    NEW_USER=$2
    NEW_PASSWORD="$3\n$3\n"

    if useradd $NEW_USER; then 
      printf $NEW_PASSWORD | passwd $NEW_USER
      printf $NEW_PASSWORD | smbpasswd -a $NEW_USER
      usermod -a -G users $NEW_USER
    else
      echo "Aborting, useradd failed" 1>&2
      exit 10
    fi
  ;;
  -d | --delete)
    DELETED_USER=$2
    
    if [ "$DELETED_USER" == "root" ]; then
      echo "Aborting, root is protected" 1>&2
      exit 20
    fi
   
    if ! userdel $DELETED_USER; then
      echo "Aborting, userdel failed" 1>&2
      exit 11
    fi
  ;;
  *)
    echo "Usage: nasusync.sh [SWITCH] [ARGUMENT]"
    echo "Creates/deletes user and gives him samba password"
    echo ""
    echo "-n | --new\t[NEW_USER] [NEW_PASSWORD]\tcreates new user"
    echo "-d | --delete\t[DELETED_USER]\tdeletes user"
    echo ""
    echo "Error codes:"
    echo "10: useradd failure"
    echo "11: userdel failure"
    echo "20: deleting root user"
  ;;
esac
