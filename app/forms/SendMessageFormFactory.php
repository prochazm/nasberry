<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Tomaj\Form\Renderer\BootstrapRenderer;
use Tomaj\Form\Renderer\BootstrapInlineRenderer;
use App\Model\ChatManager;
use Tomaj\Form\Renderer\BootstrapVerticalRenderer;

class SendMessageFormFactory{
    
    private $factory;
    private $chatManager;
    private $user;
    
    public function __construct(FormFactory $factory, User $user, ChatManager $chatManager){
        $this->factory = $factory;
        $this->chatManager = $chatManager;
        $this->user = $user;
    }
    
    public function create(callable $onSuccess){
        $form = $this->factory->create();
        $form->setRenderer(new BootstrapVerticalRenderer());
        
        $form->addTextArea('message', 'Message:')
            ->setRequired('You have to type something fisrt.')
            ->addRule(FORM::MAX_LENGTH, 'This is too long, 512 characters is maximum',512);
        $form->addSubmit('send', 'Send');
        
        $form->onSuccess[] = function(Form $form, $values) use ($onSuccess) {
            $this->chatManager->addNew($this->user->id, $values->message);   
            
            $onSuccess();
        };
        
        return $form;
    }
}