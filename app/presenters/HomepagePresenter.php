<?php

namespace App\Presenters;

use Nette;
use App\Forms\SendMessageFormFactory;
use App\Model\ChatManager;
use App\Model\UserManager;


class HomepagePresenter extends BasePresenter
{
    private $sendMessageFactory;
    private $chatManager;
    private $userManager;

    public function __construct(ChatManager $chatManager, UserManager $userManager, SendMessageFormFactory $sendMessageFactory){
        $this->chatManager = $chatManager;
        $this->userManager = $userManager;
        $this->sendMessageFactory = $sendMessageFactory;
    }


    public function actionDefault(){
        $this->template->messages = $this->chatManager->getLastXMessages(10);
        $this->template->userManager = $this->userManager;
    }

    protected function createComponentSendMessageForm()
    {
        return $this->sendMessageFactory->create(function () {
            $this->redirect('Homepage:');
        });
    }

}
