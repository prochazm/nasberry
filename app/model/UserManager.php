<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
//use Nette\Mail\IMailer;
use Nette\Mail\SendmailMailer;
use Nette\Utils\Random;
use Nette\Database\Table\ActiveRow;
use Nette\Security\Identity;
use Nette\Database\Context;


/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;

	const
		TABLE_NAME = 'users',
		COLUMN_ID = 'id',
		COLUMN_NAME = 'username',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_EMAIL = 'email',
		COLUMN_ROLE = 'role',
		COLUMN_ENABLED = 'enabled',
	    COLUMN_SETTINGS = 'setting_id';

	
	private $mailer;
	private $database;
	private $tokenManager;
	private $userSettingManager;


	public function __construct(Context $database, 
	        UserSettingManager $userSettingManager,
	        TokenManager $tokenManager){
		$this->database = $database;
		$this->tokenManager = $tokenManager;
		$this->userSettingManager = $userSettingManager;
	}
	
	/**
	 * Returns user specified by its ID
	 * @param int $id
	 * @return Nette\Database\Table\ActiveRow
	 */
	public function getUser($id){
		return $this->database->table(self::TABLE_NAME)
			->where(self::COLUMN_ID, $id)
			->fetch();
	}
	
	/**
	 * Returns user with matching name
	 * @param string $username
	 * @return Nette\Database\Table\ActiveRow
	 */
	public function getUserByName($username){
	    return $this->database->table(self::TABLE_NAME)
	    ->where(self::COLUMN_NAME, $username)
	    ->fetch();
	}
	
	/**
	 * Finds user by his id and returns in settings
	 * @param userId
	 * @return settingsId
	 */
	public function getSettings($userId){
	    return $this->database->table(self::TABLE_NAME)
	       ->where(self::COLUMN_ID, $userId)
	       ->fetch()[self::COLUMN_SETTINGS];
	}
	
	/**
	 * Checks if user is enabled/exists, returns the result, his id and
	 * error message if any
	 * @param int $id
	 * @return array(bool, string)
	 */
	public function isEnabled($id){
		$user = $this->getUser($id); 
		$isValid = false;
		$message = "";
		
		if(!$user){
			$message = "No such user.";
		} elseif($user[self::COLUMN_ENABLED]){
			$message =  "User enabled already.";
		} else {
			$isValid = true;
		}
		
		return array($isValid, $user[self::COLUMN_NAME], $message);
	}
	
	/**
	 * Sets password for user defined by id
	 * @param int $id
	 * @param string $pwd
	 */
	public function setPassword($id, $pwd) {
		$user = $this->getUser($id);
		$user->update([
        		self::COLUMN_PASSWORD_HASH => Passwords::hash($pwd),
        		self::COLUMN_ENABLED => true,					
		   ]);
	}
	
	/**
	 * Returns nicknames of all activated user accounts
	 */
	public function getAllNames(){
	    return $this->database->table(self::TABLE_NAME)
	               ->where(self::COLUMN_ENABLED, true)
	               ->fetchPairs(self::COLUMN_NAME, self::COLUMN_NAME);
    }
    
	/**
	 * Returns users password hash
	 * @param $id
	 * @return $hash
	 */
	public function getPasswordHash($id) {
	    return $this->getUser($id)[self::COLUMN_PASSWORD_HASH];
	}
	
	/**
	 * Adds user to host system and sets his samba password
	 * @param int $id
	 * @param string $pwd
	 */
	public function addAsSysUser($id, $pwd){
		$user = $this->getUser($id);
		exec('sudo ../shell/nasusync.sh -n ' . $user[self::COLUMN_NAME] . ' ' . $pwd);
	}
	
	/**
	 * Tells UserSettingManager to create new settings row and assigns
	 * it to user specified by id
	 * @param unknown $id
	 */
	public function setNewSettings($id){
	    $settingsId = $this->userSettingManager->create($id, self::COLUMN_ID);
	    $user = $this->getUser($id);
	    $user->update([
	           self::COLUMN_SETTINGS => $settingsId,     
	       ]);
	}
	
	/**
	 * Returns user whith exact name and mail address
	 * @param $username
	 * @param $email
	 * @return $user
	 */
	public function getUserByNameAndMail($username, $email){
	    return $this->database->table(self::TABLE_NAME)
	       ->where(self::COLUMN_NAME, $username)
	       ->where(self::COLUMN_EMAIL, $email)
	       ->fetch();
	}
	
	/**
	 * disables user
	 * @param $id
	 * @return void
	 */
	public function disable($id){
	    $this->getUser($id)->update([self::COLUMN_ENABLED => false]);
	}
	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		$row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_NAME, $username)->fetch();

		if (!$row) {
			throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);

		} elseif (!$row[self::COLUMN_ENABLED]){
			throw new Nette\Security\AuthenticationException('User not activated.', self::NOT_APPROVED);
			
		} elseif (!Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

		} elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
			$row->update([
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
			]);
		}
		
		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}


	/**
	 * Adds new user.
	 * @param  string $username
	 * @param  string $email
	 * @param  bool $isAdmin
	 * @throws DuplicateNameException
	 */
	public function add($username, $email, $isAdmin)
	{
		try {
			$this->database->table(self::TABLE_NAME)->insert([
					self::COLUMN_NAME => $username,
					self::COLUMN_EMAIL => $email,
					self::COLUMN_ROLE => ($isAdmin) ? 'admin' : 'user',
			        self::COLUMN_SETTINGS => '1',
			]);
			
			$user = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_NAME, $username)->fetch();
			
			$this->tokenManager->createToken($user[self::COLUMN_ID]);	
			$this->tokenManager->sendActivationMail($user[self::COLUMN_ID]);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}
}



class DuplicateNameException extends \Exception
{}
