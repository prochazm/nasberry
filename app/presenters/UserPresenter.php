<?php

namespace App\Presenters;

use Nette;
use Nette\Security;
use App\Model\UserManager;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;
use Nette\Security\Passwords;
use App\Model\UserSettingManager;

class UserPresenter extends BasePresenter {
	private $userManager;
	private $userSettingManager;
	
	public function __construct(UserManager $userManager, UserSettingManager $userSettingManager){
		$this->userManager = $userManager;
		$this->userSettingManager = $userSettingManager;
	}
	
	protected function createComponentPwdChangeForm(){
		$form = new Form();
		
		$form->setRenderer(new BootstrapRenderer());
		$form->addPassword('old_password', 'Old password:');
		$form->addPassword('password', 'New password:')
			->setRequired('Type new password.')
			->addRule(Form::MIN_LENGTH, 'Use atleast 8 characters', 8)
		    ->addRule(Form::NOT_EQUAL, 'Use different password.', $form['old_password']);
		$form->addPassword('verify', 'New password again:')
			->setRequired('Repeat the password.')
			->addRule(Form::EQUAL, 'Passwords do not match', $form['password']);
		$form->addSubmit('enter', 'Change password');
		$form->onSuccess[] = [$this, 'pwdChangeFormSucceeded'];
		
		return $form;
	}
	
	public function pwdChangeFormSucceeded(Form $form, array $values){
	    $id = $this->getUser()->getId();
	    
	    if (!Passwords::verify($values['old_password'], $this->userManager->getPasswordHash($id))) {
	        $form->addError('Wrong password.');
	        return; 
	    }
	    
	    $this->userManager->setPassword($id, $values['password']);
		$this->userManager->addAsSysUser($id, $values['password']);		
		$this->flashMessage('Password changed successfuly.', 'success');
		$this->redirect('Homepage:');
	}
	
	protected function createComponentAdminSettingsForm(){
	    $form = new Form();
	    
	    $form->setRenderer(new BootstrapRenderer());
	    $form->addCheckbox('isSysbarVisible', 'Sysbar collapsed')
	       ->setDefaultValue($this->userSettingManager->isSysbarCollapsed($this->user->id));
	    $form->addSubmit('enter', 'Enter');
	    $form->onSuccess[] = [$this, 'userSettingsFormSucceeded']; 
	       
	    return $form;
	}
	
	public function adminSettingsFormSucceeded(Form $form, array $values){
	    $this->userSettingManager->setSysbarCollapsed($values['isSysbarVisible'], $this->user->id);
	}
}