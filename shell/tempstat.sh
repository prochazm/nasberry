#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"
 
while true
do
  TEMP=$(cat /sys/class/thermal/thermal_zone0/temp)
  printf "$(($TEMP/1000))" > ../www/var/tempstat.txt

  sleep 1
done  
