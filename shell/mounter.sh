#!/bin/bash
CONNECTION_STRING="-u nasadmin -pv6SLJXtR nasdb -sN "

getParam(){
  if [[ $2 = "FS" ]]; then
    echo ${1%%:*}
  fi

  if echo $1 | grep -q $2; then 
    tmp=${1#*.$2=\"}
    tmp=${tmp%%\"*}
    
    case $tmp in *.*)
      tmp=$(echo $tmp | sed -e "s/\./ /g");; 
    esac

    echo $tmp
  fi
}
         
isMountable(){ #Reads from db by uuid, if drive's not found insert it, return it's mountable state
  UUID=$1
  LABEL=$2
  MOUNTABLE=$(mysql $CONNECTION_STRING --execute="SELECT mountable FROM drives WHERE uuid='$UUID';")   
  
  if [[ -z $MOUNTABLE ]]; then
    mysql $CONNECTION_STRING --execute="INSERT INTO drives (mountable, mounted, uuid, label) values (1, 0, '$UUID', '$LABEL');" 
    MOUNTABLE=1
  fi

  echo $MOUNTABLE
}

getNotMountable(){ #Returns all drives where mountable == 0
  echo $(mysql $CONNECTION_STRING --execute="SELECT uuid FROM drives WHERE mountable=0;")
} 

setMountable(){ #takes only uuid, sets drive to mountable
  UUID=$1

  mysql $CONNECTION_STRING --execute="UPDATE drives SET mountable=1 WHERE uuid='$UUID';"
}

getMounted(){ #Gives all drives which should be mounted acording to DB
  echo $(mysql $CONNECTION_STRING --execute="SELECT uuid FROM drives WHERE mounted=1;")
}

setMounted(){ #Sets drive mounted or !mounted in DB
  UUID=$1
  VAL=$2
   
  mysql $CONNECTION_STRING --execute="UPDATE drives SET mounted=$VAL WHERE uuid='$UUID';"
}

tryMount(){ #Iterate through unmounted drives, mount mountable ones and sets them mounted in DB
  declare -a drives=($(blkid | grep /dev/sd | sed -e 's/  */./g'))
  
  for i in "${drives[@]}"; do
    FILESYS=$(getParam $i FS)
    UUID=$(getParam $i UUID)
    LABEL=$(getParam $i LABEL)
    
    [ $(isMountable "$UUID") -eq 0 ] && continue
    
    if ! df | grep -q $FILESYS; then
      if [[ ! -d "/media/$UUID" ]]; then
        mkdir "/media/$UUID"
      fi 
      mount $FILESYS "/media/$UUID" #-o umask=000,gid=users
      chmod -R 777 "/media/$UUID"
      if [ $? -eq 0 ]; then
        setMounted "$UUID" 1
      fi
    fi
  done
}

forcedDisconnectCheck(){ #Sets drives as unmounted in case someone physically DC'd them without umount
  IFS=' ' read -a drives <<< $(getMounted)
  for i in "${drives[@]}"; do
    if ! df | grep -q "/media/$i"; then
      setMounted "$i" 0
    fi
  done
}

mountableResetCheck(){ #check if !mountable drives are unmounted, if so sets them as mountable
  IFS=' ' read -a drives <<< $(getNotMountable)
  for i in "${drives[@]}"; do
    if ! blkid | grep -q "$i"; then
      setMountable "$i"
    fi
  done
}

while(true); do 
  sleep 4
  tryMount
  sleep 1
  forcedDisconnectCheck
  mountableResetCheck
done
