<?php

namespace App\Model;

use Nette;



class SysbarManager
{
	public function getCpu(){
		return file('./var/cpustat.txt');
	}
	
	public function getRam(){
		return file('./var/memstat.txt');
	}
	
	public function getSwap(){
		return file('./var/swapstat.txt');
	}
	
	public function getTemp(){
		return file('./var/tempstat.txt');
	}
}