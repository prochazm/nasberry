<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;
use App\Model;


class SignUpFormFactory
{
	use Nette\SmartObject;

	const PASSWORD_MIN_LENGTH = 7;

	/** @var FormFactory */
	private $factory;

	/** @var Model\UserManager */
	private $userManager;


	public function __construct(FormFactory $factory, Model\UserManager $userManager)
	{
		$this->factory = $factory;
		$this->userManager = $userManager;
	}


	/**
	 * @return Form
	 */
	public function create(callable $onSuccess)
	{
		$form = $this->factory->create();
		$form->setRenderer(new BootstrapRenderer());
		$form->addText('username', 'Username:')
			->setRequired('Please pick a username.');

		$form->addText('email', 'E-mail:')	
			->setRequired('Please enter an e-mail.')
			->addRule($form::EMAIL);
		
		$form->addCheckbox('isAdmin', 'Admin account?');
		
		$form->addSubmit('send', 'Sign up');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
			try {
				$this->userManager->add($values->username, $values->email, $values->isAdmin);
			} catch (Model\DuplicateNameException $e) {
				$form->addError('Username is already taken.');
				return;
			}
			$onSuccess();
		};
		return $form;
	}

}
