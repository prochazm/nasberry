<?php 

namespace App\Presenters;

use Nette\Security\Passwords;
use App\Model\TokenManager;
use App\Model\UserManager;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapRenderer;

class TokenPresenter extends BasePresenter{
    
	private $userManager;
	private $tokenManager;
	
	private $token;
	private $userId;
	
	public function __construct(UserManager $userManager, TokenManager $tokenManager){
		$this->userManager = $userManager;
		$this->tokenManager = $tokenManager;
	}
	
	public function actionCheck($t){
		$this->user->logout();
		$this->token = $t;

		list($isValidToken, $this->userId, $errorMessageToken) = $this->tokenManager->checkToken($this->token);
		list($isValidUser, $this->template->userName, $errorMessageUser) = $this->userManager->isEnabled($this->userId);

		if (!$isValidToken){
			$this->flashMessage($errorMessageToken, "danger");
		} 
		
		if (!$isValidUser){
			$this->flashMessage($errorMessageUser, "danger");
		}
		
		if (!$isValidToken | !$isValidUser){
			$this->redirect('Homepage:');
		}
		
	}
	
	protected function createComponentActivationForm()
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());
		$form->addPassword('password', 'Password:')
			->setRequired('Pick a password')
			->addRule(Form::MIN_LENGTH, 'Use atleast 8 characters', 8);
		$form->addPassword('verify', 'Password again:')
			->setRequired('You need to repeat the password')
			->addRule(Form::EQUAL, 'Passwords don\'t match', $form['password']);
		$form->addSubmit('enter', 'Activate');
		$form->onSuccess[] = [$this, 'activationFormSucceeded'];
		return $form;
	}
	
	public function activationFormSucceeded(Form $form, $values)
	{
		$password = $values["password"];
		$this->userManager->setNewSettings($this->userId);
		$this->userManager->setPassword($this->userId, $password);
		$this->userManager->addAsSysUser($this->userId, $password);
		$this->tokenManager->discardToken($this->token);
		$this->flashMessage("Account activated!", "success");
		$this->redirect('Homepage:');
	}
}