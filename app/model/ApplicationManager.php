<?php

namespace App\Model;

use Nette,
    Nette\Database\Context,
    Nette\Database\Table\ActiveRow;

class ApplicationManager {
    
    const
        TABLE_NAME = 'applications',
        COLUMN_ID= 'id',
        COLUMN_NAME= 'username',
        COLUMN_EMAIL= 'email',
        COLUMN_IS_VALID= 'isvalid';
    
    private $database;
    
    public function __construct(Context $database){
        $this->database = $database;
    }
    
    public function add($name, $mail){
        $this->database->table(self::TABLE_NAME)->insert([
                self::COLUMN_NAME => $name,
                self::COLUMN_EMAIL => $mail,
                self::COLUMN_IS_VALID => true,
        ]);
    }
    
    public function get($id){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_ID, $id)
            ->fetch();
    }
    
    public function getValidByName($name){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_NAME, $name)
            ->where(self::COLUMN_IS_VALID, true)
            ->fetchAll();
    }
    
    public function getAllValid(){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_IS_VALID, true)
            ->fetchAll();
    }
    
    public function setInvalid($id){
        $this->get($id)->update([self::COLUMN_IS_VALID => false]);
    }
}