<?php

namespace App\Presenters;

use Nette,
    Nette\Application\UI\Form,
    App\Model\ApplicationManager,
    App\Model\UserManager;
use Tomaj\Form\Renderer\BootstrapRenderer;

class ApplicationPresenter extends BasePresenter {
    
    private $applicationManager;
    private $userManager;
    
    public function __construct(ApplicationManager $applicationManager, UserManager $userManager){
        $this->applicationManager = $applicationManager;
        $this->userManager = $userManager;
    }
    
    public function actionAccept($id){
        $application = $this->applicationManager->get($id)->toArray();
        $this->userManager->add($application[1], $application[2], false);
        $this->applicationManager->setInvalid($id);
        $this->flashMessage('Application [' . $id . '] accepted.', 'info');
        $this->redirect('Application:show');
    }
    
    public function renderShow(){
        $this->template->applications = $this->applicationManager->getAllValid();
    }
    
    protected function createComponentApplyForm(){
        $form = new Form; 
        $form->setRenderer(new BootstrapRenderer());
        
        $form->addText('username', 'Nickname: ')
            ->setRequired('Pick your nickname.')
            ->addRule(Form::MIN_LENGTH, 'Use atleast 3 characters', 3);
        $form->addEmail('email', 'Email: ')
            ->setRequired('Write your email.');
        $form->addSubmit('enter', 'Apply');
        $form->onSuccess[] = [$this, 'applyFormSucceeded'];
        return $form;
    }
    
    public function applyFormSucceeded(Form $form, array $values){
        list($username, $email) = array_values($values);
        
        if ($this->applicationManager->getValidByName($username)) {
            $this->flashMessage('Someone already applied with this nickname.', 'danger');
            $this->redirect('Application:apply');
        }
        
        if ($this->userManager->getUserByName($username)) {
            $this->flashMessage('This nickname is already taken.', 'danger');
            $this->redirect('Application:apply');
        }
        
        $this->flashMessage('You applied successfully, wait for admin\'s decission.', 'success');
        $this->applicationManager->add($username, $email);
        $this->redirect('Homepage:');
    }
    
    private function discardApplication($id, $message){
        $this->applicationManager->setInvalid($id);
        $this->template->applications = $this->applicationManager->getAllValid();
        $this->flashMessage('Application #' . $id . ' ' . $message . '.', 'info');
        $this->redrawControl('table');
        $this->redrawControl('flash');
    }
    
    public function handleAccept($id){
        if (!$this->isAjax()){
            return;
        }
        
        $user = $this->applicationManager->get($id);
        $this->userManager->add($user['username'], $user['email'], false);
        $this->discardApplication($id, 'accepted');
    }
    
    public function handleReject($id){
        if (!$this->isAjax()){
            return;
        }
        
        $this->discardApplication($id, 'rejected');
    }
    
    public function handleRefresh(){
        if (!$this->isAjax()){
            return;
        }
        
        $this->template->applications = $this->applicationManager->getAllValid();
        $this->redrawControl('table');
    }
}