<?php

namespace App\Model;

use Nette,
    Nette\Database\Table;
use Nette\Database\Context;

/**
 * User settings management.
 */
class UserSettingManager {
    
    const 
        TABLE_NAME = 'user_setting',
        COLUMN_ID = 'id',
        COLUMN_USER_ID = 'user_id',
        COLUMN_SYSB_COLLAPSED = 'sysbarIsCollapsed';
    
        /** @var Nette\Database\Context */
        private $database;
        
        public function __construct(Context $database){
            $this->database = $database;
        }
        
        public function create($userId){
            $row = $this->database->table(self::TABLE_NAME)->insert([
                    self::COLUMN_USER_ID => $userId,
                    ]);
            
            return $row->id;
        }
        
        public function isSysbarCollapsed($userId){
            return $this->database->table(self::TABLE_NAME)
                ->where(self::COLUMN_USER_ID, $userId)
                ->fetch()[self::COLUMN_SYSB_COLLAPSED];
        }
        
        public function setSysbarCollapsed($isCollapsed, $userId){
            $row = $this->database->table(self::TABLE_NAME)
                ->where(self::COLUMN_USER_ID, $userId)
                ->fetch();
            
            $row->update([
                self::COLUMN_SYSB_COLLAPSED => $isCollapsed,    
            ]);
        }
}